import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;

public class MainMenu extends JPanel implements ActionListener {
    static int score = 0;
    private JButton playButton, quitButton;
    private StellarCrush gameWindow;
    private JPanel mainPanel, panelPlayButton, panelQuitButton, panelLabel, forButton;
    private JLabel nameLabel;
    private String title, text;
    private boolean outcome;
    private int textSize;

    public void menu() {
        setFocusable(true);
        setBackground(Color.BLACK);
        setForeground(Color.WHITE);
        setDoubleBuffered(true);

        Dimension mainPanelSize = new Dimension(1024, 768);
        mainPanel = new JPanel();
        mainPanel.setBackground(Color.BLACK);
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setPreferredSize(mainPanelSize);

        Font font = new Font(null, Font.ITALIC, textSize);
        nameLabel = new JLabel(title);
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        nameLabel.setFont(font);
        nameLabel.setForeground(Color.GREEN);

        panelLabel = new JPanel();
        Dimension forPanelLabel = new Dimension(1024, 280);
        panelLabel.setPreferredSize(forPanelLabel);
        panelLabel.setBackground(Color.BLACK);

        if (!outcome) {
            JLabel loseLabel = new JLabel(text);
            loseLabel.setHorizontalAlignment(JLabel.CENTER);
            loseLabel.setVerticalAlignment(JLabel.BOTTOM);
            loseLabel.setFont(font);
            loseLabel.setForeground(Color.GREEN);

            panelLabel.add(nameLabel);
            panelLabel.add(loseLabel);
            this.score = 0;
        } else {
            panelLabel.add(nameLabel);
        }

        forButton = new JPanel();
        Dimension forButtonSize = new Dimension(1024, 468);
        forButton.setPreferredSize(forButtonSize);
        forButton.setLayout(new BorderLayout());
        forButton.setBackground(Color.BLACK);
        forButton.setBorder(new EmptyBorder(100, 300,180, 300));

        Font fontButton = new Font(null, Font.PLAIN, 60);
        playButton = new JButton("Play");
        playButton.setFont(fontButton);
        quitButton = new JButton("Quit");
        quitButton.setFont(fontButton);

        panelPlayButton = new JPanel();
        panelQuitButton = new JPanel();
        Dimension forButtons = new Dimension(400, 90);
        panelPlayButton.setBackground(Color.WHITE);
        panelPlayButton.setPreferredSize(forButtons);
        panelPlayButton.setLayout(new BorderLayout());
        panelQuitButton.setBackground(Color.YELLOW);
        panelQuitButton.setPreferredSize(forButtons);
        panelQuitButton.setLayout(new BorderLayout());
        panelPlayButton.add(playButton);
        panelQuitButton.add(quitButton);

        forButton.add(BorderLayout.NORTH, panelPlayButton);
        forButton.add(BorderLayout.SOUTH,  panelQuitButton);

        mainPanel.add(BorderLayout.CENTER, forButton);
        mainPanel.add(BorderLayout.NORTH, panelLabel);
        add(mainPanel);

        playButton.addActionListener(this);
        quitButton.addActionListener(this);
    }

    public MainMenu(StellarCrush gameWindow, boolean outcome) {
        this.outcome = outcome;
        if (outcome) {
            title = "Stellar Crush";
            textSize = 160;
        } else {
            title = "   You lose   ";
            text = "Your score is " + this.score;
            textSize = 100;
        }
        menu();
        this.gameWindow = gameWindow;
    }


    @Override
    public void actionPerformed(ActionEvent event) {
        Object source = event.getSource();
        if (source == playButton) {
            gameWindow.play();
        } else if (source == quitButton) {
            System.exit(0);
        }
    }
}
