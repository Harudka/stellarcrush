import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;

public class GameObjectLibrary {
	// Class for defining various game objects, and putting them together to
	// create content
	// for the game world. Default assumption is objects face in the direction
	// of their velocity, and are spherical.

	// UNIVERSE CONSTANTS - TUNED BY HAND FOR RANDOM GENERATION
	//private static final double ASTEROID_RADIUS = 0.5; // Location of asteroid
	// belt for random
	// initialization


	protected Collection<GameObjInterface> objects;
	private Collection<GameObjInterface> oldObjects;
	private Collection<GameObjInterface> newObjects;

	public void update() {
		updateObjects();
		removeObjects();
		setObjects();
	}

	public void keyReleased(KeyEvent e) {}
	public void keyPressed(KeyEvent e) {}

	public void render(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;

		renderActors(g2d);

		Toolkit.getDefaultToolkit().sync();
		g.dispose();
	}

	public GameObjectLibrary(){
		objects = new ArrayList<GameObjInterface>();
		oldObjects = new ArrayList<GameObjInterface>();
		newObjects = new ArrayList<GameObjInterface>();
	}

	private void renderActors(Graphics2D g2d) {
		GameObjInterface obj;
		for (Iterator<GameObjInterface> i = objects.iterator(); i.hasNext(); ) {
			obj = i.next();
			if (obj instanceof GameObject) {
				((GameObject) obj).draw(g2d);
			}
		}
	}

	public void setObject(GameObjInterface obj, int x, int y) {

		if (obj instanceof GameObject) {
			((GameObject) obj).setLocation(x,y);
		}
		obj.setWorld(this);
		newObjects.add(obj);
	}

	public void removeObjects(GameObjInterface obj) {
		oldObjects.add(obj);
	}

	private void updateObjects() {
		for (Iterator<GameObjInterface> i = objects.iterator(); i.hasNext(); ) {
			i.next().update();
		}
	}

	private void removeObjects() {
		for (Iterator<GameObjInterface> i = oldObjects.iterator(); i.hasNext(); ) {
			objects.remove(i.next());
		}
		oldObjects.clear();
	}

	private void setObjects() {
		for (Iterator<GameObjInterface> i = newObjects.iterator(); i.hasNext(); ) {
			objects.add(i.next());
		}
		newObjects.clear();
	}

}
