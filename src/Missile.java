
import java.awt.*;

public class Missile extends GameObject {

    private static int velocity = 16;

    public String getImageName() {
        return "missile.png";
    }

    public void update() {
        y -= velocity;
        if (y < -8) {
            world.removeObjects(this);
        }
    }

}
