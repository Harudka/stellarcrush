
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Iterator;

public class GameState extends GameObjectLibrary{
    // Class representing the game state and implementing main game loop update step.

    private PlayerObject player;
    private static int counter = 0;


    public void keyPressed(KeyEvent e) {
        player.keyPressed(e);
    }

    public void keyReleased(KeyEvent e) {
        player.keyReleased(e);
    }

    public GameState(){
        player = new PlayerObject();

        setObject(player, 512, 680);
        setObject(new AsteroidSpawner(), 0, 0);
    }

    public void update() {
        super.update();
        checkCollisions();
    }

    private void checkCollisions()  {
        GameObjInterface a, b;
        Rectangle aRect;

        for (Iterator<GameObjInterface> i = objects.iterator(); i.hasNext();) {
            a = i.next();
            if (a instanceof GameObject) {
                aRect = ((GameObject) a).getBounds();

                for (Iterator<GameObjInterface> j = objects.iterator(); j.hasNext();) {
                    b = j.next();
                    if (b instanceof GameObject) {
                        if (a != b) {
                            if (aRect.intersects(((GameObject) b).getBounds())) {
                                System.out.println("Collision! " + a + ":" + b);

                                if (a instanceof Missile
                                        & b instanceof Asteroid) {
                                    removeObjects(a);
                                    removeObjects(b);
                                    setObject(new Explosion(),
                                            ((GameObject) b).getX(),
                                            ((GameObject) b).getY());
                                    MainMenu.score += 100;
                                }

                                if (a instanceof Asteroid
                                        & b instanceof PlayerObject) {
                                    GamePanel.innerSwitch = true;
                                    removeObjects(a);
                                    removeObjects(b);
                                    setObject(new Explosion(),
                                            ((GameObject) b).getX(),
                                            ((GameObject) b).getY());

                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
