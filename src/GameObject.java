import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameObject extends GameObjInterface{
	// Default implementation of a game object
    protected Image image;
    protected int x = 0;
    protected int y = 0;
    public float scale = 1.0f;
    protected int imageWidth;
    protected int imageHeight;



   public GameObject(){
       image = new ImageIcon(this.getClass().getResource(getImageName())).getImage();
       imageWidth = image.getWidth(null);
       imageHeight = image.getHeight(null);
    }

   public void setLocation(int x, int y){
       this.x = x;
       this.y = y;
   }

   public void update(){

   }
    public String getImageName() {
        return null;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return (int) (imageWidth * scale);
    }

    public int getHeight() {
        return (int) (imageHeight * scale);
    }

   public void draw(Graphics2D g2d) {
       g2d.drawImage(image, x, y, this.getWidth(), this.getHeight(), null);
   }

    public Rectangle getBounds() {
        return new Rectangle(x, y, getWidth(), getHeight());
    }
}
