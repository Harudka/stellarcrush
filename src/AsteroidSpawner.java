
import java.util.Random;

public class AsteroidSpawner extends GameObjInterface {
    Random generator;

    public AsteroidSpawner() {
        generator = new Random();
    }

    public void update() {
        int r = generator.nextInt(1000);
        int x = generator.nextInt(700);

        if (r < 25) {
            world.setObject(new Asteroid(), x, -50);
        }
    }
}
