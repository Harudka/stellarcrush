
import java.awt.*;
import java.awt.event.KeyEvent;


public class PlayerObject extends GameObject {

    private int dx;
    private int dy;

    private static int velocity = 8;

    public String getImageName() {
        return "spacecraft.png";
    }

    public void update() {
        x += dx;
        y += dy;
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT) {
            dx = -velocity;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = velocity;
        }

        if (key == KeyEvent.VK_UP) {
            dy = -velocity;
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = velocity;
        }

        if (key == KeyEvent.VK_SPACE) {
            fire();
        }
    }

    public void fire() {
        world.setObject(new Missile(), x, y);
    }

    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_UP) {
            dy = 0;
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = 0;
        }
    }


}

