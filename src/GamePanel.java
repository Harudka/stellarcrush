import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@SuppressWarnings("serial")
public class GamePanel extends JPanel implements Runnable, ActionListener {
	private GameObjectLibrary gameState;
	public static boolean outerSwitch = false;
	public static boolean innerSwitch = false;
    private Thread animator;
        
    private final int DELAY = 50;

    public GamePanel(GameState gameState) {
    	this.gameState = gameState;
	
    	addKeyListener(new TAdapter());
    	setFocusable(true);
        setBackground(Color.BLACK);
        setForeground(Color.WHITE);
        setDoubleBuffered(true);
    }

	public void addNotify() {
        super.addNotify();
        
        animator = new Thread(this);
        animator.start();
    }

    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D)g;
        
        gameState.render(g2d);
        
        Toolkit.getDefaultToolkit().sync();
        g.dispose();		
    }

    public void run() {
        long beforeTime, timeDiff, sleep;

        beforeTime = System.currentTimeMillis();

        while (!animator.isInterrupted()) {
            gameState.update();
            repaint();
            timeDiff = System.currentTimeMillis() - beforeTime;
            sleep = DELAY - timeDiff;

            if (sleep < 0)
                sleep = 2;
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                System.out.println("interrupted");
            }
            if(outerSwitch == true){
                outerSwitch = false;
                innerSwitch = false;
                new StellarCrush(false);
                animator.interrupt();
            }
            beforeTime = System.currentTimeMillis();
        }
    }
        
    private class TAdapter extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            gameState.keyPressed(e);
        }

        public void keyReleased(KeyEvent e) {
            gameState.keyReleased(e);
        }
    }

	public void actionPerformed(ActionEvent ae) {}
}