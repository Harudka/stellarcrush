
import java.awt.*;
import java.util.Random;

public class Asteroid extends GameObject{

    private static int velocity = 6;

    public String getImageName() {
        return "asteroid.png";
    }

    public void update(){
        y += velocity;
        if (y > 800) {
            world.removeObjects(this);
            System.out.println("delete");
        }
    }
}

