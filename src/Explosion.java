
import java.awt.*;

public class Explosion extends GameObject {
    private int lifetime = 0;

    public Explosion() {
        super();
        scale = 2.0f;
    }

    public void update() {
        lifetime += 1;
        scale += 0.2;
        if (lifetime > 4) {
            world.removeObjects(this);
            if(GamePanel.innerSwitch == true){
                GamePanel.outerSwitch = true;
            }
        }
    }

    public String getImageName() {
        return "explosion.png";
    }

}
